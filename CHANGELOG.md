## v0.0.2 (2023-05-29)

### Feat

- add feature bar

### Fix

- correct values returned from foo & bar

## v0.0.1 (2023-05-29)

### Feat

- add feature foo
